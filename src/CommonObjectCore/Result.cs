﻿namespace CommonObjectCore
{
    public class Result
    {
        public ResultType Type { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
        public string TechnicalMessage { get; set; }
    }

    public class Result<T>
    {
        public ResultType Type { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
        public string TechnicalMessage { get; set; }
        public T Object { get; set; }
    }

}
