﻿namespace CommonObjectCore
{
    using System;
    using System.Globalization;
    using Newtonsoft.Json;

    public static class Helpers
    {
        public static string ToPersian(this DateTime dateTime, PersianDateTimeFormat persianDateTimeFormat = PersianDateTimeFormat.Date)
        {
            var pc = new PersianCalendar();

            var year = pc.GetYear(dateTime).ToString();
            var month = pc.GetMonth(dateTime).ToString().PadLeft(2, '0');
            var day = pc.GetDayOfMonth(dateTime).ToString().PadLeft(2, '0');

            switch (persianDateTimeFormat)
            {
                case PersianDateTimeFormat.Date:
                    return $"{year}/{month}/{day}";
                case PersianDateTimeFormat.DateAndTime:
                    var time = dateTime.TimeOfDay;
                    return $"{year}/{month}/{day} {dateTime:HH':'mm':'ss}";
            }

            return $"{year}/{month}/{day}";
        }

        public static DateTime PersianToGregorian(this string persianDate)
        {
            var persian = persianDate.Split('/');

            var year = int.Parse(persian[0]);
            var month = int.Parse(persian[1]);
            var day = int.Parse(persian[2]);

            var pc = new PersianCalendar();

            var dateTime = pc.ToDateTime(year, month, day, 0, 0, 0, 0);

            return dateTime;
        }

        public static string ToJson<T>(this T obj)
        {
            var json = JsonConvert.ToString(obj);

            return json;
        }
    }
}
